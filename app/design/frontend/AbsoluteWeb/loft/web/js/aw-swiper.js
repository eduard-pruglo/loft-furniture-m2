/*
 * Absolute Web Intellectual Property
 *
 * @copyright    Copyright (c) 1999-2021 Absolute Web, Inc. (http://www.absoluteweb.com)
 * @author       Absolute Web
 * @license      http://www.absoluteweb.com/license-agreement/  Single domain license
 * @terms of use http://www.absoluteweb.com/terms-of-use/
 */

define(['jquery', 'swiper-lib'], function($, Swiper) {
    return function(config, element) {
        var sliderEl = $(element)[0],
            sliderInstance,
            navigationButtons,
            navThumbsSlider,
            button;

        config['init'] = false;

        if (config.hasOwnProperty('productGallery')) {
            navThumbsSlider = $('.product-thumbs')[0].swiper;

            config.thumbs = {
                'swiper': navThumbsSlider
            };
        }

        sliderInstance = new Swiper(sliderEl, config);

        sliderInstance.on('init', function () {
            for (navigationButtons in config.navigation) {
                if (Object.hasOwnProperty.call(config.navigation, navigationButtons)) {
                    button = config.navigation[navigationButtons];

                    $(button).addClass('swiper-button-initialized');
                }
            }
        });

        sliderInstance.init();
    };
});