/*
* Absolute Web Intellectual Property
*
* @copyright    Copyright (c) 1999-2020 Absolute Web, Inc. (http://www.absoluteweb.com)
* @author       Absolute Web
* @license      http://www.absoluteweb.com/license-agreement/  Single domain license
* @terms of use http://www.absoluteweb.com/terms-of-use/
*/

define([
    'jquery'
],function ($) {
    'use strict';

    $.widget('aw.smoothScroll', {
        options: {
            anchorLink: '.anchor-btn',
            speed: 500
        },

        /**
         * Initializes class instance.
         */
        _init: function() {
            var anchorAttr,
                _this = this;

            $(this.options.anchorLink).click(function (e) {
                e.preventDefault();

                anchorAttr = $(this).attr('href');
                _this._scrollTo(anchorAttr);
            });
        },

        /**
         *  @param {String} anchor.
         *
         *  Smooth scroll to element with ID
         */
        _scrollTo: function (anchor) {
            $('html, body').animate({
                scrollTop: $(anchor).offset().top
            }, this.options.speed, function () {
                location.hash = anchor;
            });
        }
    });

    return $.aw.smoothScroll;
});