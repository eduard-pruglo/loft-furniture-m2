'use strict';

define([
    'jquery'
], function ($) {
    'use strict';

    $.widget('aw.counterForFiles', {
        options: {
            inputFile: '.input-file',
            countFile: '.input-file-count',
            labelFile: '.label-file',
            active: 'active'
        },

        /**
         * @private
         */
        _init: function() {
            this._bind();
        },

        /**
         * @private
         */
        _bind: function() {
            var _this = this,
                input = _this.options.inputFile,
                count;

            $(input).on('change', function () {
                count = $(this).get(0).files.length;

                _this.ckeckAttachedCount(count);
            });
        },

        ckeckAttachedCount: function(count) {
            var _this = this;

            if (count > 0) {
                $(_this.options.countFile).text(count);
                $(_this.options.countFile).addClass(_this.options.active);
            } else {
                $(_this.options.countFile).removeClass(_this.options.active);
            }
        }
    });

    return $.aw.counterForFiles;
});

