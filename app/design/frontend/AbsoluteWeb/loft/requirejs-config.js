/**
 * Absolute Web Intellectual Property
 *
 * @copyright    Copyright © 1999-2021 Absolute Web, Inc. (http://www.absoluteweb.com)
 * @author       Absolute Web
 * @license      http://www.absoluteweb.com/license-agreement/ Single domain license
 * @terms of use http://www.absoluteweb.com/terms-of-use/
*/

'use strict';

// eslint-disable-next-line no-unused-vars
var config = {
    map: {
        '*': {
            'swiper-lib': 'js/vendor/swiper.min',
            'aw-swiper': 'js/aw-swiper',
            'aw-smooth-scroll': 'js/aw-smooth-scroll'
        }
    }
};
